# Copyright 2013 University of Bath

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 NAME

EPrints::Plugin::Storage::HCP - use the REST interface to the Hitachi Content Platform to store files for EPrints

=head1 DESCRIPTION

See L<EPrints::Plugin::Storage> for available methods.

=head1 METHODS

=over 4

=cut

package EPrints::Plugin::Storage::HCP;

use Encode;    
use URI;
use URI::Escape;
 
use EPrints::Plugin::Storage;

require LWP::UserAgent;
use LWP::MediaTypes qw( guess_media_type );
require HTTP::Request;
require HTTP::Cookies;
require LWP::MediaTypes;
use Net::SSLeay;

use constant BUF_SIZE => 65536;
 
@ISA = ( "EPrints::Plugin::Storage" );

use strict;

sub create_ua
{
	my ($self,$fileobj) = @_;	
	# get configuration details and return an LWP::UserAgent object with no URI set, to be used to make the request
	# try this with a temporary file if HTTP::Request doesn't work out
	my $ua = LWP::UserAgent->new;
	$ua->ssl_opts( SSL_verify_mode => Net::SSLeay::VERIFY_NONE(),
		verify_hostname => 0 ); # don't check validity of server cert

	my( $path, $fn ) = $self->_filename( $fileobj );
	return undef if !defined $path;

	my $repository = $self->{session}->get_repository;

	# HCP automatically creates directory hierarchy as needed, so no requirement to do this piece by piece
	$self->{_url}->{$fileobj} = $repository->config("extension","HCP_storage","rest_base_url") . $path . $fn; # path and fn both start with /
	$repository->log("URL: " . $self->{_url}->{$fileobj} ) if ($self->{debug}); # for testing

	# set cookie for authentication
	my $key = $repository->config("extension","HCP_storage","auth_cookie_name");
	my $val = $repository->config("extension","HCP_storage","auth_cookie_value");
	my $domain = $repository->config("extension","HCP_storage","auth_cookie_domain");
	$repository->log("Key: " . $key . "; Value: " . $val . "; Domain: " . $domain ) if ($self->{debug}); # for testing

	# sets the cookie with a 10 minute expiration time
	my $cjar = HTTP::Cookies->new({});
	# sets the cookie with a 10 minute expiration time (a lot longer than is likely to be needed for a single web transaction, but would cope with (e.g.) local clock on EPrints server being a little out of sync with HCP
	$cjar->set_cookie( undef, $key, $val, '/', $domain, 443, 0, 0, 600, 1 );
	$ua->cookie_jar($cjar);

	$self->{_ua}->{$fileobj} = $ua;
	return 1;
}
   
sub new
{
	my( $class, %params ) = @_;
  
	my $self = $class->SUPER::new( %params );
 
	$self->{name} = "HCP Storage Plugin";
	$self->{debug} = 0;
	my $repository = $self->{session}->get_repository;

	$self->{storage_class} = "hitachi_content_platform_storage_via_rest";
	$self->{position} = 100;
	$self->{HCP_delete_mode} = 2; # default value - EPrints deletion same as HCP deletion
	if (defined $repository->config("extension","HCP_storage","delete_mode"))
	{
		$self->{delete_mode} = $repository->config("extension","HCP_storage","delete_mode") ;
	}
  
	return $self;
}

sub open_write
{
	my( $self, $fileobj, $offset ) = @_;
	my $repository = $self->{session}->get_repository;
	if (create_ua( $self, $fileobj))
	{
		$self->{_request}->{$fileobj} = new HTTP::Request('PUT',$self->{_url}->{$fileobj});
		$self->{_empty}->{$fileobj} = 1;
		return 1;
	}
	else
	{
		$repository->log("Could not define LWP user agent: aborting file save\n");
		return 0;
	}
}
 
sub write
{
	my( $self, $fileobj, $buffer ) = @_;

	# curl command is curl -kiT -b hcp-ns-auth=encrypteduserid:=encryptedpassword -H 'Content-Type: content type' -H 'Host: hostname'  https://namespace.tenant.hostname/rest/path/filename
	# directory name is EPrints userid of depositor, need to determine content type
	# options in curl command: -k insecure (do not check SSL certs), -i include http header in output, -T upload file
	
	my $request = $self->{_request}->{$fileobj};
	$request->add_content( $buffer );
	return 1;
}

sub close_write
{
	my( $self, $fileobj ) = @_;
	my $ua = $self->{_ua}->{$fileobj};
	my $response = $ua->request($self->{_request}->{$fileobj});

	$self->{session}->get_repository->log( $response->status_line ) if ($self->{debug}); # for testing
	return $response->is_success;
}
  
sub retrieve
{
	my( $self, $fileobj, $sourceid, $offset, $n, $f ) = @_;

	# what follows is based on http://trac.eprints.org/eprints/browser/trunk/system/perl_lib/EPrints/Plugin/Storage/AmazonS3.pm?rev=4327; the commented lines are an older version which would probably work but needs to work out how to pass the data to a filehandle which does not point to a file...
	my $repository = $self->{session}->get_repository;
	if (create_ua( $self, $fileobj))
	{
		my $ua = $self->{_ua}->{$fileobj};
		my $request = new HTTP::Request('GET',$self->{_url}->{$fileobj});	
		my $response = $ua->request( $request,$f ); # feed the response to the callback function
		if( $response->is_error )
		{
			$repository->log( $response->as_string );
		}
		return $response->is_success;
	}
	else
	{
		$repository->log("Could not define LWP user agent: aborting file save\n");
		return 0;
	}
}
  
sub delete
{
       	my( $self, $fileobj, $revision ) = @_;
	my $delmode = $self->{HCP_delete_mode};
	my $repository = $self->{session}->get_repository;
       	
	if ($delmode == 0)
	{
       		# should always return failure as this is supposed to be a WORM archive
       		$self->{session}->get_repository->log( "Cannot delete archived items" );
       		return 0;
	}
	elsif ($delmode == 1)
	{
		# should always return success (remove from EPrints but not HCP)
		return 1;
	}
	else
	{
		# attempt to carry out deletion
		my $response;
		if (create_ua( $self, $fileobj))
		{
			my $ua = $self->{_ua}->{$fileobj};
			$response = $ua->delete($self->{_url}->{$fileobj});
			if( $response->is_error )
			{
				$repository->log( $response->as_string );
			}
		}
		else
		{
			$repository->log("Could not define LWP user agent: aborting file save\n");
			return 0;
		}

		if ($delmode == 2)
		{
			# what happens in EPrints should be the same as at the HCP
			return $response->is_success;
		}
		else
		{
			# always return success, allowing deletion of objects from EPrints interface while retaining them on the HCP
			return 1;
		}
	}
}  

sub _filename {
	my ($self,$fileobj,$filename) = @_;

	my $path = "/";
	my $filename = "";
	if( defined $fileobj )
	{
		$path .= $fileobj->get_id;
		$path .= "/file/";
		$filename = URI::Escape::uri_escape( $fileobj->get_value( "filename" ) );
	}

	return ($path,$filename);
}

=back

=cut

1;  

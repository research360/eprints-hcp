# Copyright 2013 University of Bath

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

# file containing configuration for HCP storage using the HCP REST interface
# place in archive cfg.d directory where it will be automatically loaded

# to set storage policy amend lib/storage/default.xml and set HCP as the storage mechanism

$c->{extension}->{HCP_storage}->{auth_cookie_name} = "hcp-ns-auth"; # the standard name for cookies used for authentication to the HCP is hcp-ns-auth
$c->{extension}->{HCP_storage}->{auth_cookie_value} = "characters=:characters"; # hashed values for username and password to access the namespace to be used
$c->{extension}->{HCP_storage}->{auth_cookie_domain} = "domain"; # the domain for the namespace to be used - should be the main domain for the whole HCP not the specific namespace
$c->{extension}->{HCP_storage}->{rest_base_url} = "https://namespace.tenant.domain/rest"; # the main REST URL for the namespace to be used - shouldn't end with a / as that's provided by EPrints as part of the file path
$c->{extension}->{HCP_storage}->{delete_mode} = 2; # the deletion mode to use: 0 always report failure (treat EPrints and the HCP as a WORM archive), 1 always report success (allow deletion from the EPrints interface while treating the HCP as a WORM archive), 2 make the deletion request to the HCP and report the result (so that the deletion settings of the HCP will be "replicated" in the EPrints interface), 3 make the deletion request to the HCP but always report success (thus enabling access to be removed to items which the HCP will not delete)


This software is a plugin for the [EPrints][] repository system,
allowing storage of content in a [Hitachi Content Platform][HCP]
object store. It was produced as part of the [Jisc][]-funded
[Research360 project][] at the [University of Bath][].

* [Read the report - "Research360: EPrints Integration with the Hitachi Content Platform"][report]

[EPrints]: http://www.eprints.org/
[Jisc]: http://www.jisc.ac.uk/
[Research360 project]: http://blogs.bath.ac.uk/research360/about/
[University of Bath]: http://www.bath.ac.uk/
[HCP]: http://www.hds.com/products/file-and-content/content-platform/
[report]: http://opus.bath.ac.uk/35532/

## How to Install

1.  Set up EPrints, ideally using SSL. This should also set up an
    EPrints userID whose home directory is the EPrints installation
    location (`$EPRINTS_HOME`, in this document - which can be omitted
    if it's your current directory when running through these
    instructions). The perl libraries needed should already be installed
    either as part of the EPrints installation or in the core perl
    installation.

2.  If LWP::Protocol::https is not installed, then install it. If you do
    not do this, then the following error will appear in the apache
    error log:

    > Can't locate object method "ssl\_opts" via package
    > "LWP::UserAgent".

3.  Set up a tenant and namespace to use for storage from the archive on
    the HCP. The module will set up the same directory structure as
    EPrints natively uses to manage its files on the remote machine
    within this namespace. A user needs to be created that can access
    this namespace using the REST API, and then hashed versions of the
    credentials used by this user need to be calculated, which are used
    to authenticate by the module. The UNIX command to create the hashed
    credentials is:

        echo hcp-ns-auth=`echo -n username | base64`:`echo -n password | md5sum` | awk '{print $1}'

    replacing username and password with the appropriate values.

4.  Unzip the archive in a convenient location (e.g. `/tmp`). This is
    most conveniently done as the EPrints user, because otherwise the
    remaining tasks will also require re-setting file ownerships as
    root.

5.  As EPrints, copy HCP.pm to
    `$EPRINTS_HOME/perl_lib/EPrints/Plugin/Storage/HCP.pm`. Then change
    the group ownership of the file to www-data:

        chown eprints:www-data $EPRINTS_HOME/perl_lib/EPrints/Plugin/Storage/HCP.pm

    to match the other files in the directory.

6.  Modify `$EPRINTS_HOME/lib/storage/default.xml` and configure storage
    mechanism to be "HCP" for the archive or mime types which should use
    the HCP.

7.  Modify HCP.pl to match local HCP installation. The information
    needed is the URL to access the namespace, and the access
    credentials. The deletion mode for the module should also be set.
    There are four options:

    -   0: Always report failure (i.e. do not attempt to delete files
        from the archive); items will not disappear from either the HCP
        or EPrints - this means that both EPrints and the HCP are
        treated as WORM archives
    -   1: Always report success (also not attempting to delete files
        from the archive); items will disappear from the EPrints
        interface but not from the HCP, so that EPrints is a read/write
        archive while the HCP is treated as a WORM archive
    -   2: (Default) Make a deletion request to the HCP and report the
        result; how this behaves will depend on the deletion settings
        for the file on the HCP, and it will be removed from EPrints
        precisely when it has successfully been removed from the HCP, so
        that the deletion settings of the HCP will be "replicated" in
        the EPrints interface
    -   3: Make a deletion request to the HCP but always report success;
        items will always disappear from the EPrints interface but may
        not be deleted from the HCP, thus enabling access to be removed
        to items which the HCP will not delete

8.  As EPrints, copy the modified HCP.pl to
    `$EPRINTS_HOME/archives/[archive-id]/cfg/cfg.d/HCP.pl`, replacing
    [archive-id] with the archive ID to be used; a copy will need to be
    placed in each archive's configuration where it is desired to use
    the HCP for storage (the HCP.pl files can be customised differently
    for each archive).

9.  As root, restart apache. (This may not be necessary, but it doesn't
    hurt.) If you want to see more information about the activities of
    the HCP module in the logs, then change line 77 of HCP.pm to

        $self->{debug} = 1;

    before restarting apache.

## Copyright & licensing

Copyright 2013 University of Bath

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.

